<?php

namespace App\Models\AssetUpload;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use DateTime;
use DateInterval;
use DatePeriod;

class AssetUploadTable extends Model
{
    use HasFactory;

    protected $table = 'asset_upload';

    //add AssetUpload
    public function saveAssetUpload($request)
    {
        //to get all request values
        $data = $request->all();
        // dd($data);
        $folder =  public_path('uploads');
        $validated = request()->validate([
                    'file' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf,xlx,xlsx,csv,doc,docx|max:2048',
                    ]);
        $format = request()->file->getClientOriginalExtension();
        $size = request()->file->getSize();
        $fileName = time().'.'.request()->file->getClientOriginalExtension();
        request()->file->move(public_path('uploads'), $fileName);
        $name = public_path('uploads').'/'.$fileName;
        $fileReturn = 'uploads/'.$fileName;
        $timezone = 'Asia/Calcutta';
        $date = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone($timezone));
        $id = DB::table('asset_upload')->insertGetId(
            ['asset_name' => $data['fileName'],
            'asset_path' => $fileReturn,
            'asset_size' => $size,
            'asset_format' => $format,
            'uploaded_on' => $date->format('Y-m-d H:i:s'),
            'uploaded_by' => session('userId'),
            ]
        );
           
        return $id;
    }

    public function getAllAssets(){
        $data = DB::table('asset_upload')->get();
        return $data;
    }

    public function getDistFileSize(){
        $data = DB::table('asset_upload')->select('asset_size')
        ->distinct()->get();
        return $data;
    }

    public function displayAllAssets()
    {
        $data = DB::table('asset_upload')->join('user', 'user.user_id', '=', 'asset_upload.uploaded_by')->get();
        return $data;
    }

    public function getFilterdAssets($params){
        $data = $params->all();
        $query = DB::table('asset_upload')
                ->join('user', 'user.user_id', '=', 'asset_upload.uploaded_by');
        if(isset($data['fileName']) && $data['fileName'])
            $query->where('asset_upload.asset_id','=',$data['fileName']);
        if(isset($data['fileFormat']) && $data['fileFormat'])
            $query->where('asset_upload.asset_format','=',$data['fileFormat']);
        if(isset($data['fileSize']) && $data['fileSize'])
            $query->where('asset_upload.asset_size','=',$data['fileSize']);
        if(isset($data['uploadedBy']) && $data['uploadedBy'])
            $query->where('asset_upload.uploaded_by','=',$data['uploadedBy']);
        $data1 = $query->get();
        return $data1;
    }

    public function pieChartData()
    {
        $data = DB::table('asset_upload')
         ->select(DB::raw('SUM(CASE WHEN asset_format = "pdf" THEN 1 ELSE 0 END) As pdf'), 
         DB::raw('SUM(CASE WHEN asset_format = "docx" THEN 1 ELSE 0 END) As docx'),
         DB::raw('SUM(CASE WHEN asset_format = "jpeg" THEN 1 ELSE 0 END) As jpeg'),
         DB::raw('SUM(CASE WHEN asset_format = "jpg" THEN 1 ELSE 0 END) As jpg'),
         DB::raw('SUM(CASE WHEN asset_format = "png" THEN 1 ELSE 0 END) As png'), 
         DB::raw('SUM(CASE WHEN asset_format = "doc" THEN 1 ELSE 0 END) As doc'),
         DB::raw('SUM(CASE WHEN asset_format = "gif" THEN 1 ELSE 0 END) As gif'),
         DB::raw('SUM(CASE WHEN asset_format = "svg" THEN 1 ELSE 0 END) As svg'),
         DB::raw('SUM(CASE WHEN asset_format = "xlx" THEN 1 ELSE 0 END) As xlx'),
         DB::raw('SUM(CASE WHEN asset_format = "xlsx" THEN 1 ELSE 0 END) As xlsx'),
         DB::raw('SUM(CASE WHEN asset_format = "csv" THEN 1 ELSE 0 END) As csv') )->get();
        return $data;
    }
}
