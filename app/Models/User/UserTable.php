<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Service\UserService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use DateTime;
use DateInterval;
use DatePeriod;

class UserTable extends Model
{
    use HasFactory;

    protected $table = 'user';

    //add user
    public function saveUser($request)
    {
        //to get all request values
        $data = $request->all();
        // dd($data);
        $timezone = 'Asia/Calcutta';
        $date = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone($timezone));
        if ($request->input('nameSignUp')!=null && trim($request->input('nameSignUp')) != '') {
            $id = DB::table('user')->insertGetId(
                ['user_name' => $data['nameSignUp'],
                'user_email' => $data['emailSignUp'],
                'user_mobile' => $data['mobileNo'],
                'user_password' => Hash::make($data['passwordSignUp']),
                'registered_on' => $date->format('Y-m-d H:i:s'),
                ]
            );

           
        }
        return $id;
    }

    // Validate User login
    public function validateLogin($params)
    {
        $data = $params->all();
        // dd($data);
        $result = json_decode(DB::table('user')
        ->where('user_email','=', $data['email'])
        ->get(),true);
        if(count($result)>0)
        {
            $hashedPassword = $result[0]['user_password'];
            if (Hash::check($data['password'], $hashedPassword)) {
                session(['name' => $result[0]['user_name']]);
                session(['mobileNo' => $result[0]['user_mobile']]);
                session(['email' => $result[0]['user_email']]);
                session(['userId' => $result[0]['user_id']]);
                session(['login' => true]);
                return 1;
            }
            else{
                return 2;
            }
        }
        else
        {
            return 0;
        }
    }

    public function getAllUsers(){
        $data = DB::table('user')->get();
        return $data;
    }
}
