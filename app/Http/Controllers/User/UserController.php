<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\UserService;
use Redirect;
use Session;

class UserController extends Controller
{
    public function add(Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $service = new UserService();
            $result = $service->saveUser($request);
            return Redirect::route('login')->with('status', $result);

        }
        else
        {
            return view('login');
        }
    }
}
