<?php

namespace App\Http\Controllers\AssetUpload;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\AssetUploadService;
use App\Service\UserService;
use Redirect;
use DataTables;

class AssetUploadController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(['csrf'])->except('getFilterdAssets');
    // }
    public function index(Request $request)
    {
        if(session('login')==true)
        {
            if ($request->isMethod('post')){
                $service = new AssetUploadService();
                $add = $service->saveAssetUpload($request);
                return Redirect::route('assetupload.index')->with('status', $add);
            }
            else{
                return view('assetupload.index');
            }
        }
        else
            return Redirect::to('login')->with('status', 'Please Login');
    }

    public function assets()
    {
        if(session('login')==true)
        {
            $service = new AssetUploadService();
            $user = new UserService();
            $add = $service->getAllAssets();
            $fileSize = $service->getDistFileSize();
            $uploadedBy = $user->getAllUsers();
            return view('assetupload.assets',array('uploadedBy'=>$uploadedBy, 'result'=>$add, "fileName" => $add , "fileSize" => $fileSize));
        }
        else
            return Redirect::to('login')->with('status', 'Please Login');
    }

    public function getFilterdAssets(Request $request)
    {
        $service = new AssetUploadService();
        $data = $service->getFilterdAssets($request);
        return $data;
    }

    // Get all the Role list
    public function displayAllAssets(Request $request)
    {
        $service = new AssetUploadService();
        $data = $service->displayAllAssets($request);
        return DataTables::of($data)
            ->make(true);
    }
}
