<?php
/*
Author : Sudarmathi M
Date : 26 Mar 2021
Desc : Controller for Login screen
*/
namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Service\UserService;

class LoginController extends Controller
{
    //View Login main screen
     public function index()
     {
         return view('login.index');
     }

     //validate login
     public function validateLogin(Request $request)
    {
            $service = new UserService();
            $login = $service->validateLogin($request);
            if(trim($login)==1)
            {
                return Redirect::to('/dashboard');
            }
            elseif(trim($login)==2)
            { 
                return Redirect::route('login')->with('status', 'Please Check Your Login Credentials!');
            }
            else
            { 
                return Redirect::route('login')->with('status', 'Login Failed');
            }
    }


    //Logout
    public function logout(Request $request){
        // dd(session('login'));
        if($request->isMethod('post') && session('login')==true){
            $request->session()->flush();
            $request->session()->regenerate();
            Session::flush();
            return Redirect::to('login'); // redirect the user to the login screen
        }else{
            if(session('login')== null){
                return Redirect::to('login');
            }
            return redirect()->back();
        }
    }
 
    
}
