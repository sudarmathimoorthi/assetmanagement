<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\AssetUploadService;

class DashboardController extends Controller
{
    public function index()
    {
        if(session('login')==true)
        {
            $service = new AssetUploadService();
            $data = $service->pieChartData();
            // print_r($data);die;
            return view('dashboard.index',array('result'=>$data));
        }
        else
            return Redirect::to('login')->with('status', 'Please Login');
    }

}
