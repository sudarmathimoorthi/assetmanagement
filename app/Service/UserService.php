<?php

namespace App\Service;
use App\Models\User\UserTable;
// use App\EventLog\EventLog;
use DB;
use Redirect;

class UserService
{
   
    public function saveUser($request)
    {
    	$data =  $request->all();
    	DB::beginTransaction();
    	try {
			$model = new UserTable();
        	$addsector = $model->saveUser($request);
			if($addsector>0){
				DB::commit();
				$msg = 'User Added Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
    
    //Get validate User Details
	public function validateLogin($params)
	{
		$model = new UserTable();
        $result = $model->validateLogin($params);
        return $result;
	}

	public function getAllUsers(){
        $model = new UserTable();
        $result = $model->getAllUsers();
        return $result;
    }
	
}

?>