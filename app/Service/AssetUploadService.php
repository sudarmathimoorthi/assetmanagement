<?php

namespace App\Service;
use App\Models\AssetUpload\AssetUploadTable;
use DB;
use Redirect;

class AssetUploadService
{
   
    public function saveAssetUpload($request)
    {
    	$data =  $request->all();
    	DB::beginTransaction();
    	try {
			$model = new AssetUploadTable();
        	$result = $model->saveAssetUpload($request);
			if($result>0){
				DB::commit();
				$msg = 'File Uploaded Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
    }
    
    public function getAllAssets(){
        $model = new AssetUploadTable();
        $result = $model->getAllAssets();
        return $result;
    }

    public function getDistFileSize(){
        $model = new AssetUploadTable();
        $result = $model->getDistFileSize();
        return $result;
    }

    public function getFilterdAssets($params){
        $model = new AssetUploadTable();
        $result = $model->getFilterdAssets($params);
        return $result;
    }

    public function pieChartData(){
        $model = new AssetUploadTable();
        $result = $model->pieChartData();
        return $result;
    }

    public function displayAllAssets(){
        $model = new AssetUploadTable();
        $result = $model->displayAllAssets();
        return $result;
    }
	
}

?>