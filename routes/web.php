<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.index');
});

Route::get('login', [ 'as' => 'login', 'uses' => 'App\Http\Controllers\Login\LoginController@index']);
Route::post('/login/validate', 'App\Http\Controllers\Login\LoginController@validateLogin');
Route::match(['get','post'],'/logout', 'App\Http\Controllers\Login\LoginController@logout');
//User Module

// Route::get('/user/add', 'User\UserController@add');
Route::post('/user/add', 'App\Http\Controllers\User\UserController@add');


Route::get('/dashboard', 'App\Http\Controllers\Dashboard\DashboardController@index')->name('dashboard.index');
Route::post('/displayAllAssets', 'App\Http\Controllers\AssetUpload\AssetUploadController@displayAllAssets');
Route::get('/assetupload', 'App\Http\Controllers\AssetUpload\AssetUploadController@index')->name('assetupload.index');
Route::post('/assetupload', 'App\Http\Controllers\AssetUpload\AssetUploadController@index');

Route::get('/asset', 'App\Http\Controllers\AssetUpload\AssetUploadController@assets')->name('assetupload.assets');
Route::post('/getFilterdAssets', 'App\Http\Controllers\AssetUpload\AssetUploadController@getFilterdAssets');