CREATE TABLE `dckap`.`user` ( `user_id` INT NOT NULL AUTO_INCREMENT , `user_name` VARCHAR(255) NOT NULL , `user_mobile` VARCHAR(255) NOT NULL , `user_email` VARCHAR(255) NULL , `user_password` VARCHAR(255) NOT NULL , `registered_on` DATETIME NULL , PRIMARY KEY (`user_id`)) ENGINE = InnoDB;

CREATE TABLE `asset_upload` (
 `asset_id` int(11) NOT NULL AUTO_INCREMENT,
 `asset_name` varchar(255) NOT NULL,
 `asset_path` varchar(255) NOT NULL,
 `asset_size` varchar(255) DEFAULT NULL,
 `asset_format` varchar(255) DEFAULT NULL,
 `uploaded_by` int(11) DEFAULT NULL,
 `uploaded_on` datetime DEFAULT NULL,
 PRIMARY KEY (`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1