<!-- 
    Author             : Sudarmathi M
    Date               : 26 Mar 2021
    Description        : Dashboard
    Last Modified Date : 26 Mar 2021
    Last Modified Name : Sudarmathi M
-->
<style>

.middle {
  position: absolute;
  top: 50%;
  left: 90%;
  transform: translate(-50%, -50%);
  text-align: center;
}
.modebar{
      display: none !important;
}
</style>

@extends('layouts.main')
<script src='https://cdn.plot.ly/plotly-latest.min.js'></script>
@section('content')

<!-- <div class="middle"> <h1 style="font-size: -webkit-xxx-large;">Coming Soon...</h1></div> -->
<div class="container-fluid ">
    {{-- <div class="row">
       <center> <h1>Coming Soon...</h1>  </center>
    </div> --}}
    <div class="col-md-6 mt-3">
      <div class="card">
        <div id='myDiv'><!-- Plotly chart will be drawn inside this DIV --></div>
      </div>
    </div>
    <div class="col-md-12 mt-3">
        <div class="card-body">
            <div class="table-responsive p-t-10">
              <table id="uplList" class="display" style="width:100%">
                <thead>
                  <tr>
                      <th style="display: none">Asset Id</th>
                      <th>Asset Name</th>
                      <th>Asset Size</th>
                      <th>Asset Format</th>
                      <th>Uploaded By</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  <tr>
                      <th style="display: none">Asset Id</th>
                      <th>Asset Name</th>
                      <th>Asset Size</th>
                      <th>Asset Format</th>
                      <th>Uploaded Name</th>
                  </tr>
                  </tfoot>
              </table>
            </div>
          </div>
    </div>
  </div>
<div class="row" style="height:390px;">
</div>
<script>
var data = [{
  values: [{{$result[0]->png}}, {{$result[0]->jpg}}, {{$result[0]->jpeg}},{{$result[0]->doc}}, {{$result[0]->docx}}, {{$result[0]->xlx}}, {{$result[0]->xlsx}}, {{$result[0]->pdf}}, {{$result[0]->csv}},{{$result[0]->gif}}, {{$result[0]->svg}}],
  labels: ['Png', 'Jpg', 'Jpeg','Doc', 'Docx', 'Xlx', 'Xlsx','Pdf', 'Csv', 'Gif', 'Svg'],
  type: 'pie',
  
}];

var layout = {
  title: 'Number of image format percent in uploaded files',
  height: 400,
  width: 500
};

Plotly.newPlot('myDiv', data, layout);
$(document).ready(function() {
      displayAllAssets()
    });
function displayAllAssets()
  {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#uplList').DataTable({
      processing: true,
      destroy: true,
      serverSide: true,
      scrollX: false,
      autoWidth: false,
      ajax: {
        url: '{{ url("displayAllAssets") }}',
        type: 'POST',
      },
      columns: [
                    
                    { data: 'asset_id', name: 'asset_id', visible: false },
                    { data: 'asset_name', name: 'asset_name',className : 'firstcaps' },
                    { data: 'asset_size', name: 'asset_size' },
                    { data: 'asset_format', name: 'asset_format' },
                    { data: 'user_name', name: 'user_name' },
                ],
      order: [
        [0, 'desc']
      ]
    });
  }
</script>
@endsection