<!-- 
    Author : Sudarmathi M
    Date : 26 Mar 2021
    Desc : Footer page
-->
<div class="footer" style="float:right">
    <div class="copyright">
        <p>Copyright &copy; 2021 All Rights Reserved. </p>
    </div>
</div>