<div id="mySidebar" class="sidebar">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
  <br>
  <a href="/dashboard">Dashboard</a>
  <a href="/assetupload">Asset Upload</a>
  <a href="/asset">Assets</a>
  <form action="/logout" name="logoutForm" id="logoutForm" method="POST" >
    @csrf
    <button type="submit" class="btn text-center" style="-webkit-appearance: none !important;background-color: #f1f1f1;">Logout</button>
  </form>
</div>