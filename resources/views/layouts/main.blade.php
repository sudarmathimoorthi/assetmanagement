<!-- 
    Author : Sudarmathi M
    Date : 26 Mar 2021
    Desc : Main page to integrate all the pages
-->
<!DOCTYPE html>
<html>
    <head>

        @include('layoutsections.header')
        @include('layoutsections.scripts')
    </head>
    <style>
        .mandatory{color: #f30f00;}
        .firstcaps{
            text-transform:capitalize;
        }
        .form-control {
            height: 30px !important;
            min-height: 35px !important;
        }
        .number-right{
            text-align:right;
        }
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        .pace-inactive {
            display: none;
        }

        .pace .pace-progress {
            background: #29d;
            position: fixed;
            z-index: 2000;
            top: 0;
            right: 100%;
            width: 100%;
            height: 2px;
        }
        .bg-light {
            background-color: #00bcd4!important;
        }
    </style>
    <body style="overflow-x: hidden;">

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper"  style="min-height:50vw;">
        @include('layoutsections.navheader')

      <!-- Sidebar starts-->
        @include('layoutsections.sidebar')
      <!-- Sidebar ends -->

      <!--**********************************
            Content body start
      ***********************************-->
        <div class="content-body">
          @section('content')

          @show
        </div>
      <!--**********************************
            Content body ends
      ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!-- footer starts-->

    @include('layoutsections.footer')
      
    <!-- footer ends-->

    <!-- JavaScript files-->

    

    <!-- JavaScript files-->
    <div class="modal fade" id="modal_ajax" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width:1000px">
            <div class="modal-content" id="modal-content"></div>
        </div>
    </div>

  </body>
</html>

<script>
    function openNav() {
      document.getElementById("mySidebar").style.width = "250px";
      document.getElementById("main").style.marginLeft = "250px";
    }
    
    function closeNav() {
      document.getElementById("mySidebar").style.width = "0";
      document.getElementById("main").style.marginLeft= "0";
    }

  $(document).ready(function() {
    // $(".content-body").css("min-height", "600px");
    var path = '<?php echo Request::path();?>';
    var splitUrl = path.split('/');
    if(splitUrl[0]=="dashboard")
    {
      $("#dashboard").addClass('active');  
    }
    else if(splitUrl[0]=="branchPlan" ||splitUrl[0]=="plan" || splitUrl[0]=="shop" ||splitUrl[0]=="role"||splitUrl[0]=="admin" ||splitUrl[0]=="adminrole" ||splitUrl[0]=="employee" ||splitUrl[0]=="customer"  ||splitUrl[0]=="supplier" ||splitUrl[0]=="shopbranch" )
    {
      $("#masterul").addClass('in');
      $("#master").addClass('active');
      $("#"+splitUrl[0]).addClass('active');
    }
    else if(splitUrl[0]=="item" ||splitUrl[0]=="stock" ||splitUrl[0]=="stockUpdate" || splitUrl[0]=="itemreceive" || splitUrl[0]=="itemprice" )
    {
      $("#itemul").addClass('in');
      $("#item").addClass('active');
      $("#"+splitUrl[0]).addClass('active');
    }
    else if(splitUrl[0]=="supplierTransaction" || splitUrl[0]=="sales" || splitUrl[0]=="otherexpenses"  || splitUrl[0]=="estimation" )
    {
    $("#transactionul").addClass('in');
      $("#transaction").addClass('active');
      $("#"+splitUrl[0]).addClass('active');
    }

    else if(splitUrl[0]=="supplierTransactionHistory" || splitUrl[0]=="customerTransactionHistory" || splitUrl[0]=="needToOrdersItems" || splitUrl[0]=="fastMovingItems" || splitUrl[0]=="branchAssetAmount" )
    {
    $("#reportul").addClass('in');
      $("#report").addClass('active');
      $("#"+splitUrl[0]).addClass('active');
    }
 
  });

  ismob = true;
  ispin = true;
  isempmob = true;

  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }


  function isChar(evt){
      var inputValue = (evt.which) ? evt.which : evt.keyCode
      if(!(inputValue >= 65 && inputValue <= 120  && inputValue != 94) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
      }
  }

  function showAjaxModal(url)
  {
      // SHOWING AJAX PRELOADER IMAGE
      jQuery('#modal_ajax .modal-content').html('');

      // LOADING THE AJAX MODAL
      jQuery('#modal_ajax').modal('show', {backdrop: 'true'});
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
      // SHOW AJAX RESPONSE ON REQUEST SUCCESS
      $.ajax({
          url: url,
          method: 'post',
          success: function (response)
          {
            
            jQuery('#modal_ajax .modal-content').html(response);
            
          }
      });
  }
  </script>