<!-- 
    Author             : Sudarmathi M
    Date               : 26 Mar 2021
    Description        : Asset Upload Page
    Last Modified Date : 26 Mar 2021
    Last Modified Name : Sudarmathi M
-->
<style>


</style>
@extends('layouts.main')
@section('content')
<?php //print_r($result);die; ?>
<!-- <div class="middle"> <h1 style="font-size: -webkit-xxx-large;">Coming Soon...</h1></div> -->
<div class="container-fluid">
    {{-- @csrf --}}
        <div class="row">
            <div class="col-xl-1 col-lg-2 mt-4">
                    File Name
            </div>
            <div class="col-xl-3 col-lg-6">
                <fieldset class="form-group mt-3">
                        <select class="form-control" id="fileName">
                                <option value="">Select File Name Filter</option>
                                @foreach ($fileName as $name)
                                    <option value="{{$name->asset_id}}">{{$name->asset_name}}</option>
                                @endforeach
                            </select>
                </fieldset>
            </div>
            <div class="col-xl-1 col-lg-2 mt-4">
                    Format
            </div>
            <div class="col-xl-3 col-lg-6">
                <fieldset class="form-group mt-3">
                    <select class="form-control" id="fileFormat">
                        <option value="">Select File Format Filter</option>
                        <option value="jpeg">Jpeg</option>
                        <option value="png">Png</option>
                        <option value="jpg">Jpg</option>
                        <option value="gif">GIF</option>
                        <option value="svg">Svg</option>
                        <option value="pdf">Pdf</option>
                        <option value="xlx">xlx</option>
                        <option value="csv">CSV</option>
                        <option value="docx">Docx</option>
                        <option value="doc">Doc</option>
                        <option value="xlsx">xlsx</option>
                    </select>
                </fieldset>
            </div>
            <div class="col-xl-1 col-lg-2 mt-4">
                    File Size
            </div>
            <div class="col-xl-3 col-lg-6">
                <fieldset class="form-group mt-3">
                        <select class="form-control" id="fileSize">
                                <option value="">Select File Size Filter</option>
                                @foreach ($fileSize as $size)
                                    <option value="{{$size->asset_size}}">{{$size->asset_size}}</option>
                                @endforeach
                            </select>
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-1 col-lg-2 mt-4">
                    Uploaded By
            </div>
            <div class="col-xl-3 col-lg-6">
                <fieldset class="form-group mt-3">
                        <select class="form-control" id="uploadedBy">
                                <option value="">Select Uploaded By Filter</option>
                                @foreach ($uploadedBy as $up)
                                    <option value="{{$up->user_id}}">{{$up->user_name}}</option>
                                @endforeach
                            </select>
                </fieldset>
            </div>
            <div class="col-xl-1 col-lg-2 mt-3">
                    <button class="btn btn-success" onclick="getFilteredAssets();">Search</button> 
            </div>
        </div>
        <div class="row mt-4" id="contentData">
            @foreach ($result as $res)
              <div class="col-md-2">
                    @if($res->asset_format == 'jpeg' || $res->asset_format == 'png' || $res->asset_format == 'jpg' || $res->asset_format == 'gif')
                        <img src="{{$res->asset_path}}" alt="Test" width="200" height="200">
                    @else
                    <a href="{{$res->asset_path}}" target="_blank"><img src="https://cdn.iconscout.com/icon/free/png-256/doc-file-75-898976.png" alt="Test" width="200" height="200"></a>
                    @endif
                    <br><br><p class="text-center">{{$res->asset_name}}</p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
<script>
function getFilteredAssets()
{
    // alert($('meta[name="csrf-token"]').attr('content'))
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    $.ajax({
        url: '{{ url("getFilterdAssets") }}',
        type: "POST",
        data: {
                  fileName :$("#fileName").val(),
                  fileFormat :$("#fileFormat").val(),
                  fileSize :$("#fileSize").val(),
                  uploadedBy :$("#uploadedBy").val(),
              },
            success: function(data){
            var div = " ";
            for(var i=0; i<data.length; i++)
            {
                if(data[i]["asset_format"] == 'jpeg' || data[i]["asset_format"] == 'png' || data[i]["asset_format"] == 'jpg' || data[i]["asset_format"] == 'gif')
                {
                    div+='<div class="col-md-2">\
                        <img src="'+data[i]["asset_path"]+'" alt="Test" width="200" height="200">\
                        ';
                }
                else
                {
                    div+='<div class="col-md-2">\
                            <a href="'+data[i]["asset_path"]+'" target="_blank"><img src="https://cdn.iconscout.com/icon/free/png-256/doc-file-75-898976.png" alt="Test" width="200" height="200"></a>\
                        ';
                }
                div += '<br><br><p class="text-center">'+data[i]["asset_name"]+'</p></div>'
            }
            $("#contentData").html(div);
        }
    });
}
</script>
@endsection