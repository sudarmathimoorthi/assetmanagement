<!-- 
    Author             : Sudarmathi M
    Date               : 26 Mar 2021
    Description        : Asset Upload Page
    Last Modified Date : 26 Mar 2021
    Last Modified Name : Sudarmathi M
-->
<style>


</style>
@extends('layouts.main')

@section('content')

<!-- <div class="middle"> <h1 style="font-size: -webkit-xxx-large;">Coming Soon...</h1></div> -->
<div class="container mt-3 middle">
    @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show ml-5 mr-5 mt-4" role="alert" id="show_alert_index">
            <div class="text-center" style=""><b>
                {{ session('status') }}</b></div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <script>
            $('#show_alert_index').delay(3000).fadeOut();
        </script>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form form-horizontal" enctype="multipart/form-data" role="form" name="uploadFileForm" id="uploadFileForm" method="post" action="/assetupload/" autocomplete="off" onsubmit="validateNow();return false;">
        @csrf
        <div id="show_alert" class="mt-1" style=""></div>
        <div class="row">
            <div class="col-xl-2 col-lg-4">
            </div>
            <div class="col-xl-6 col-lg-12">
                File Name
                <fieldset class="form-group mt-3">
                    <input type="text" class="form-control" id="fileName" name="fileName">
                </fieldset>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2 col-lg-4">
            </div>
            <div class="col-xl-6 col-lg-12">
                File Upload
                <fieldset class="form-group mt-3">
                    <div id="file-upload">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/3c8dbc/fff&text=Upload File" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                            <span class="btn btn-info btn-file"><span class="fileinput-new">Select File</span><span class="fileinput-exists">Change</span>
                            <input type="file" name="file" id="file">
                            </span>
                            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="form-group has-success text-center">
            <button type="submit" class="btn btn-success" name="submit">Upload</button>
        </div>
    </form>
</div>

<script>
duplicateName = true;

function validateNow() {
    flag = validator.init({
        formId: 'uploadFileForm'
    });
    if (flag == true) {
        if (duplicateName) {
            document.getElementById('uploadFileForm').submit();
        }
    } else {
        // Swal.fire('Any fool can use a computer');
        $('#show_alert').html(flag).delay(3000).fadeOut();
        $('#show_alert').css("display", "block");
        $(".infocus").focus();
    }
}

</script>

@endsection