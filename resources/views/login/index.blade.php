<!DOCTYPE html>
<html>
  <head>
  
  @include('layoutsections.header')

  </head>
  <style>
  .mandatory{color: #f30f00;}
  .logostyle{
    color:#fff;font-size: 30px;!important;
  }
  .firstcaps{
    text-transform:capitalize;
  }
  body{
      margin-top : 30px;
  }
  </style>


<body class="h-100" data-theme-version="light" data-layout="vertical" data-nav-headerbg="color_1" data-headerbg="color_1" data-sidebar-style="full" data-sibebarbg="color_1" data-sidebar-position="static" data-header-position="static" data-container="wide" direction="ltr">

    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show ml-5 mr-5 mt-1" role="alert" id="show_alert_index" ><div class="text-center" style="font-size: 18px;"><b>
                            {{ session('status') }}</b></div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <script>$('#show_alert_index').delay(3000).fadeOut();</script>
                    @endif
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                                <a class="text-center" href="/"> <h4>LOGIN</h4></a>
                                <div id="show_alert"  class="mt-4" style=""></div>
                                <form class="mt-5 mb-5 login-input" id="loginValidate" name="loginValidate" action="login/validate" method="post" onsubmit="loginNow();return false;">
                                @csrf
                                    <div class="form-group">
                                    <label class="" for="email">Email <span class="text-danger">*</span>
                                        </label>
                                        <input type="tel" name="email" class="form-control isRequired" placeholder="Enter the Mobile Number" title="Please Enter Mobile Number">
                                    </div>
                                    <div class="form-group">
                                    <label class="" for="password">Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="password" class="form-control isRequired" placeholder="Enter the Password" title="Please Enter Password">
                                    </div>
                                    <br>
                                    <button class="btn btn-info w-100" type="submit">Sign In</button>
                                </form>
                                <p class="mt-5 login-form__footer text-center" style=""> <a href="#" data-target="#registerModal" data-toggle="modal" class="text-primary">Register Here</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="modal fade " id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="overflow-y: initial !important">
            <div class="modal-content">
                <div class="modal-header" style="border: none;">
                    <center><h4>Sign Up</h4></center>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body " >
                    <form id="signUpForm" name="signUpForm" action="/user/add" method="post" onsubmit="validateNow();return false;">
                        @csrf
                        <div class="">
                            <div class="form-group">
                                <label class="" for="nameSignUp">Name <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="nameSignUp" class="form-control isRequired" placeholder="Enter the Name" title="Please Enter Name">
                            </div>
                            <div class="form-group">
                                <label class="" for="mobileNo">Mobile Number <span class="text-danger">*</span>
                                </label>
                                <input type="tel" name="mobileNo" maxlength="10" class="form-control isRequired" placeholder="Enter the Mobile Number" title="Please Enter Mobile Number">
                            </div>
                            <div class="form-group">
                                <label class="" for="emailSignUp">Email <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="emailSignUp" class="form-control isRequired" placeholder="Enter the Email" title="Please Enter Email">
                            </div>
                            <div class="form-group">
                                <label class="" for="passwordSignUp">Password <span class="text-danger">*</span>
                                </label>
                                <input type="password" name="passwordSignUp" class="form-control isRequired" placeholder="Enter the Password" title="Please Enter Password">
                            </div>
                            <br>
                            <button class="btn btn-info w-100">Sign Up</button>
                            <br><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@include('layoutsections.scripts')
<script>
duplicateName = true;
    function validateNow() {
        $.blockUI();
        flag = deforayValidator.init({
            formId: 'loginValidate'
        });
        console.log(flag);
        if (flag == true) {
            if (duplicateName) {
                document.getElementById('loginValidate').submit();
            }
        }
        else{
            $.unblockUI();
            // Swal.fire('Any fool can use a computer');
            $('#show_alert').html(flag).delay(3000).fadeOut();
            $('#show_alert').css("display","block");
        }
    }


duplicateName = true;
function validateNow() {
    flag = validator.init({
        formId: 'signUpForm'
    });
    if (flag == true) {
        if (duplicateName) {
            // document.getElementById('signUpForm').submit();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.blockUI();
            $.ajax({
                url: "{{ url('/user/add') }}",
                method: 'post',
                data:{
                    'nameSignUp' : $('#nameSignUp').val(),
                    'emailSignUp' : $('#emailSignUp').val(),
                    'passwordSignUp' : $('#passwordSignUp').val(),
                    'mobileNo' : $('#mobileNo').val(),
                },
                success: function (data) {
                    $.unblockUI();
                    console.log(data)
                    if(data){
                        swal("Registered Successfully!");
                        setInterval(function(){
                            location.reload();
                        }, 3000);
                    }
                }
            });
        }
    }
    else{
        $.unblockUI();
        $('#show_alert').html(flag).delay(3000).fadeOut();
        $('#show_alert').css("display","block");
    }
      
}

function loginNow() {
        $.blockUI();
        flag = validator.init({
            formId: 'loginValidate'
        });
        console.log(flag);
        if (flag == true) {
            if (duplicateName) {
                document.getElementById('loginValidate').submit();
            }
        }
        else{
            $.unblockUI();
            $('#show_alert').html(flag).delay(3000).fadeOut();
            $('#show_alert').css("display","block");
        }
    }

</script>





</body>
